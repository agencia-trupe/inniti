<?php

return [

    'name'        => 'Inniti',
    'title'       => 'Inniti | Executive Search · Corporate Governance · HR',
    'description' => 'Somos parceiros de nossos clientes na busca de executivos, desenvolvimento de líderes e talentos, gestão de performance, cultura e engajamento.',
    'keywords'    => 'executive search, corporate governance, desenvolvimento organizacional, coaching, recrutamento, gestão de performance, assessment, desenvolvimento de líderes, recursos humanos, consultoria',
    'share_image' => '',
    'analytics'   => 'UA-64181906-1'

];
