<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('banners', 'App\Models\Banner');
        $router->model('home', 'App\Models\Home');
        $router->model('paginas', 'App\Models\Pagina');
        $router->model('contato', 'App\Models\Contato');
        $router->model('politica-de-privacidade', 'App\Models\Privacidade');
        $router->model('fale-conosco', 'App\Models\ContatoRecebido');
        $router->model('candidato', 'App\Models\CurriculoRecebido');
        $router->model('empresa', 'App\Models\EmpresaRecebido');
        $router->model('usuarios', 'App\Models\User');

        $router->bind('pagina', function($value) {
            return \App\Models\Pagina::whereSlug($value)->first() ?: \App::abort('404');
        });

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
