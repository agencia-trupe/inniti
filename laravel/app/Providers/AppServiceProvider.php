<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('painel.common.nav', function($view) {
            $view->with('contatos_nao_lidos', \App\Models\ContatoRecebido::naoLidos()->count());
            $view->with('empresa_nao_lidos', \App\Models\EmpresaRecebido::naoLidos()->count());
            $view->with('curriculos_nao_lidos', \App\Models\CurriculoRecebido::naoLidos()->count());
        });

        view()->composer('frontend.common.header-home', function($view) {
            $view->with('banners', \App\Models\Banner::ordenados()->get());
        });

        view()->composer('frontend.common.template', function($view) {
            $view->with('contato', \App\Models\Contato::first());
            $view->with('slogan', \App\Models\Home::first()->slogan);
            $view->with('paginas', \App\Models\Pagina::ordenados()->select('categoria', 'titulo', 'slug')->get());
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
