<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class HomeRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'slogan'         => 'required',
            'texto'          => 'required',
            'chamada1'       => 'required',
            'chamada1_frase' => 'required',
            'chamada2'       => 'required',
            'chamada2_frase' => 'required',
            'chamada3'       => 'required',
            'chamada3_frase' => 'required',
            'chamada4'       => 'required',
            'chamada4_frase' => 'required',
        ];
    }
}
