<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CurriculoRecebidoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome'               => 'required',
            'email'              => 'email|required',
            'carta_apresentacao' => 'required',
            'curriculo'          => 'required|mimes:doc,docx,pdf,txt,rtf,zip|max:4000'
        ];
    }
}
