<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\CurriculoRecebido;

class CurriculosRecebidosController extends Controller
{
    public function index()
    {
        $curriculosrecebidos = CurriculoRecebido::orderBy('id', 'DESC')->paginate(15);

        return view('painel.contato.curriculos.index', compact('curriculosrecebidos'));
    }

    public function show(CurriculoRecebido $curriculo)
    {
        $curriculo->update(['lido' => 1]);

        return view('painel.contato.curriculos.show', compact('curriculo'));
    }

    public function destroy(CurriculoRecebido $curriculo)
    {
        try {

            $curriculo->delete();
            return redirect()->route('painel.contato.candidato.index')->with('success', 'Currículo excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }
}
