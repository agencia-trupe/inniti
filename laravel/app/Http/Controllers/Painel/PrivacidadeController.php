<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PrivacidadeRequest;
use App\Http\Controllers\Controller;

use App\Models\Privacidade;

class PrivacidadeController extends Controller
{
    public function index()
    {
        $privacidade = Privacidade::first();

        return view('painel.paginas.privacidade.index', compact('privacidade'));
    }

    public function update(PrivacidadeRequest $request, Privacidade $privacidade)
    {
        try {

            $input = $request->all();

            $privacidade->update($input);
            return redirect()->route('painel.paginas.politica-de-privacidade.index')->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }
}
