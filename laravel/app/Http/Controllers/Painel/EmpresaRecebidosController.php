<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\EmpresaRecebido;

class EmpresaRecebidosController extends Controller
{
    public function index()
    {
        $contatosrecebidos = EmpresaRecebido::orderBy('id', 'DESC')->paginate(15);

        return view('painel.contato.empresa.index', compact('contatosrecebidos'));
    }

    public function show(EmpresaRecebido $contato)
    {
        $contato->update(['lido' => 1]);

        return view('painel.contato.empresa.show', compact('contato'));
    }

    public function destroy(EmpresaRecebido $contato)
    {
        try {

            $contato->delete();
            return redirect()->route('painel.contato.empresa.index')->with('success', 'Mensagem excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir mensagem: '.$e->getMessage()]);

        }
    }
}