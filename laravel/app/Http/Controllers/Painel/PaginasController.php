<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\PaginaRequest;
use App\Http\Controllers\Controller;

use App\Models\Pagina;
use App\Helpers\CropImage;

class PaginasController extends Controller
{
    private $categorias = [
        'somos-diferentes' => 'Somos Diferentes',
        'servicos'         => 'Serviços'
    ];

    private $image_config = [
        'width'  => 800,
        'height' => null,
        'upsize' => true,
        'path'   => 'assets/img/paginas/'
    ];

    public function index($categoria = null)
    {
        if (!$categoria) return redirect()->route('painel.paginas.home.index');

        $categorias = $this->categorias;
        if (!array_key_exists($categoria, $categorias)) \App::abort('404');

        $paginas = Pagina::whereCategoria($categoria)->ordenados()->get();

        return view('painel.paginas.index', compact('categorias', 'categoria', 'paginas'));
    }

    public function create($categoria)
    {
        if (!$categoria || !array_key_exists($categoria, $this->categorias)) \App::abort('404');

        return view('painel.paginas.create', compact('categoria'));
    }

    public function store(PaginaRequest $request)
    {
        try {

            $input = $request->all();

            Pagina::create($input);
            return redirect()->route('painel.paginas.index', $input['categoria'])
                             ->with('success', 'Página adicionada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar página: '.$e->getMessage()]);

        }
    }

    public function edit(Pagina $pagina)
    {
        return view('painel.paginas.edit', compact('pagina'));
    }

    public function update(PaginaRequest $request, Pagina $pagina)
    {
        try {

            $input = $request->all();

            $pagina->update($input);
            return redirect()->route('painel.paginas.index', $input['categoria'])
                             ->with('success', 'Página alterada com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar página: '.$e->getMessage()]);

        }
    }

    public function destroy(Pagina $pagina)
    {
        try {

            $categoria = $pagina->categoria;

            $pagina->delete();
            return redirect()->route('painel.paginas.index', [$categoria])
                             ->with('success', 'Página excluída com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir pagina: '.$e->getMessage()]);

        }
    }

    public function imageUpload(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'imagem' => 'image|required'
        ]);

        if ($validator->fails()) {
            $response = [
                'error'   => true,
                'message' => 'O arquivo deve ser uma imagem.'
            ];
        } else {
            $imagem   = CropImage::make('imagem', $this->image_config);
            $response = [
                'filepath' => asset($this->image_config['path'] . $imagem)
            ];
        }

        return response()->json($response);
    }
}
