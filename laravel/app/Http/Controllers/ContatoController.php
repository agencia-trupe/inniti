<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\ContatoRecebidoRequest;
use App\Http\Requests\CurriculoRecebidoRequest;
use App\Http\Requests\EmpresaRecebidoRequest;
use App\Models\Contato;
use App\Models\ContatoRecebido;
use App\Models\EmpresaRecebido;
use App\Models\CurriculoRecebido;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ContatoController extends Controller
{

    public function index($pagina = 'fale-conosco')
    {
        $contato = Contato::first();

        $paginas = ['fale-conosco', 'candidato', 'empresa'];
        if (!in_array($pagina, $paginas)) \App::abort('404');

        $view = 'frontend.contato.' . $pagina;
        return view($view, compact('contato', 'pagina'));
    }

    public function envioFaleConosco(ContatoRecebidoRequest $request)
    {
        ContatoRecebido::create($request->all());
        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.fale-conosco', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[FALE CONOSCO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return \Redirect::back()->with('success', true);
    }

    public function envioEmpresa(EmpresaRecebidoRequest $request)
    {
        EmpresaRecebido::create($request->all());
        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.empresa', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[EMPRESA] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return \Redirect::back()->with('success', true);
    }

    public function envioCandidato(CurriculoRecebidoRequest $request)
    {
        $uploadedFile      = $request->file('curriculo');
        $curriculoFileName = str_replace(' ', '-', Str::ascii($uploadedFile->getClientOriginalName()));
        $curriculoPath     = base_path('curriculos/');

        $curriculo = CurriculoRecebido::create($request->all());
        $curriculoDownloadFileName = $curriculo->id.'-'.$curriculoFileName;
        $uploadedFile->move($curriculoPath, $curriculoDownloadFileName);

        $curriculo->key = str_random(20).$curriculo->id;
        $curriculo->curriculo = $curriculoDownloadFileName;
        $curriculo->save();

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.candidato', (array) $curriculo['attributes'], function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CANDIDATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        return \Redirect::back()->with('success', true);
    }
}
