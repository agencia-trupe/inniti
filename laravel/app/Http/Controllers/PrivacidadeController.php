<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Privacidade;
use Illuminate\Http\Request;

class PrivacidadeController extends Controller
{

    public function index()
    {
        $privacidade = Privacidade::first();

        return view('frontend.privacidade', compact('privacidade'));
    }

}
