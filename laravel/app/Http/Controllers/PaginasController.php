<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Pagina;

class PaginasController extends Controller
{

    public function index($pagina = null)
    {
        $categoria  = \Route::currentRouteName();
        $collection = Pagina::whereCategoria($categoria)->ordenados();

        $paginas = $collection->get();
        $pagina  = $pagina ?: $collection->first();

        return view('frontend.paginas', compact('categoria', 'paginas', 'pagina'));
    }
}
