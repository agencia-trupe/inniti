<?php

// Front-end
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@index']);
Route::get('somos-diferentes/{pagina?}', ['as' => 'somos-diferentes', 'uses' => 'PaginasController@index']);
Route::get('servicos/{pagina?}', ['as' => 'servicos', 'uses' => 'PaginasController@index']);
Route::get('contato/{tipo?}', ['as' => 'contato', 'uses' => 'ContatoController@index']);
Route::post('contato/fale-conosco/envio', ['as' => 'contato.envio.fale-conosco', 'uses' => 'ContatoController@envioFaleConosco']);
Route::post('contato/candidato/envio', ['as' => 'contato.envio.candidato', 'uses' => 'ContatoController@envioCandidato']);
Route::post('contato/empresa/envio', ['as' => 'contato.envio.empresa', 'uses' => 'ContatoController@envioEmpresa']);
Route::get('politica-de-privacidade', ['as' => 'politica-de-privacidade', 'uses' => 'PrivacidadeController@index']);

// Download currículo
Route::get('download/{key}',
    ['as' => 'curriculo.download', function($key) {
        $curriculo = \App\Models\CurriculoRecebido::whereKey($key)->first();
        if (!$curriculo) App::abort('404');
        return response()->download(base_path('curriculos/'.$curriculo->curriculo));
    }]
);

// Painel
Route::group(['prefix' => 'painel', 'namespace' => 'Painel', 'middleware' => 'auth'], function() {
    Route::get('/', ['as' => 'painel', 'uses' => 'HomeController@index']);

    Route::resource('banners', 'BannersController');
    Route::resource('paginas/home', 'TextosHomeController');
    Route::resource('paginas/contato', 'ContatoController');
    Route::resource('paginas/politica-de-privacidade', 'PrivacidadeController');
    Route::resource('paginas', 'PaginasController', ['except' => 'show']);
    Route::get('paginas/{categoria}', ['as' => 'painel.paginas.index', 'uses' => 'PaginasController@index']);
    Route::get('paginas/{categoria}/create', ['as' => 'painel.paginas.create', 'uses' => 'PaginasController@create']);
    Route::resource('contato/fale-conosco', 'ContatosRecebidosController');
    Route::resource('contato/candidato', 'CurriculosRecebidosController');
    Route::resource('contato/empresa', 'EmpresaRecebidosController');
    Route::resource('usuarios', 'UsuariosController');

    Route::post('ckeditor-upload', 'PaginasController@imageUpload');
    Route::post('order', 'HomeController@order');
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});


// Auth
Route::group(['prefix' => 'painel', 'namespace' => 'Auth'], function() {
    Route::get('login', ['as' => 'login', 'uses' => 'AuthController@getLogin']);
    Route::post('login', ['as' => 'auth', 'uses' => 'AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@getLogout']);
});
