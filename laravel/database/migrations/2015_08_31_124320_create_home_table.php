<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('home', function (Blueprint $table) {
            $table->increments('id');
            $table->text('slogan');
            $table->text('texto');
            $table->string('chamada1');
            $table->string('chamada1_frase');
            $table->string('chamada2');
            $table->string('chamada2_frase');
            $table->string('chamada3');
            $table->string('chamada3_frase');
            $table->string('chamada4');
            $table->string('chamada4_frase');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('home');
    }
}
