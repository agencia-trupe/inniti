<?php

use Illuminate\Database\Seeder;

class ContatoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'telefone'   => '+55 11 3568 2590',
            'email'      => 'contato@inniti.com.br',
            'endereco'   => '<p>Av. Brigadeiro Faria Lima, 3144&nbsp;&middot; 30&ordm; andar</p><p>Itaim Bibi&nbsp;&middot; S&atilde;o Paulo SP&nbsp;&middot; 01451-001&nbsp;&middot; Brasil</p>',
            'linkedin'   => 'https://br.linkedin.com/company/teperman',
            'googlemaps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d117079.5550434779!2d-46.602918949999996!3d-23.506013249999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5769dddfab03%3A0x284d9c6a4f6013b6!2sAv.+Brg.+Faria+Lima%2C+3144+-+Jardim+Paulistano%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1441298504634" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>'
        ]);
    }
}
