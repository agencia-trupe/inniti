<?php

use Illuminate\Database\Seeder;

class HomeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('home')->insert([
            'slogan'         => '<p><strong>Connect</strong> to align.</p><p>Align to <strong>succeed</strong>.</p>',
            'texto'          => '<p><em>Acreditamos que o <strong>autoconhecimento </strong>&eacute; capaz de transformar empresas e profissionais. E &eacute; a partir desse entendimento que os auxiliamos a analisar e compreender o que de fato <strong>desejam</strong>, <strong>precisam </strong>e, principalmente, no que <strong>realmente acreditam</strong>.</em></p><p><em>O resultado &eacute; um <strong>perfeito alinhamento de prop&oacute;sitos</strong> entre todas as partes.</em></p>',
            'chamada1'       => 'Autenticidade e Conhecimento',
            'chamada1_frase' => 'para entender que uma empresa é feita, acima de tudo, de pessoas',
            'chamada2'       => 'Propósito e Precisão',
            'chamada2_frase' => 'para alinhar pessoas e negócios',
            'chamada3'       => 'Cumplicidade e Confiança',
            'chamada3_frase' => 'para gerar valor para todas as partes',
            'chamada4'       => 'Transparência',
            'chamada4_frase' => 'para unir objetivos e expectativas',
        ]);
    }
}
