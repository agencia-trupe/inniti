<?php

use Illuminate\Database\Seeder;

class PrivacidadeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('privacidade')->insert([
            'texto' => 'Política de Privacidade.'
        ]);
    }
}
