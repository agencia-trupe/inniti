(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.homeCycle = function() {
        var $wrapper = $('.banners');
        if (!$wrapper) return;

        $wrapper.cycle({
            slides: '>.banner',
            pagerTemplate: '<a href=#>{{slideNum}}</a>',
        });

        $wrapper.on('cycle-update-view', function (event, opts, slideOpts, currentSlide) {
            $('#banner-link-outside').attr('href', $(currentSlide).data('link'));
        });
    };

    App.mobileToggle = function() {
        var $handle = $('#mobile-toggle'),
            $nav    = $('nav.mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.inputCurriculo = function() {
        var $handle = $('#curriculo');
        if (!$handle) return;

        $handle.find('input').on('change', function(event) {
            var files  = event.target.files,
                isFile = files.length,
                text   = (isFile ? files[0].name : 'ANEXAR CURRÍCULO');

            $handle.find('span').html(text).parent().toggleClass('active', !!+isFile);
        });
    };

    App.zoomImagensPaginas = function() {
        var $wrapper = $('.paginas-texto');
        if (!$wrapper) return;

        $wrapper.find('img').on('click', function() {
            $.fancybox.open({
                href: this.src
            }, {
                padding: 10,
                scrolling: 'yes',
                fitToView: false,
                margin: 20,
            });
        });
    };

    App.init = function() {
        this.homeCycle();
        this.mobileToggle();
        this.inputCurriculo();
        this.zoomImagensPaginas();
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
