<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>

    <li class="dropdown @if(str_is('painel.paginas*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Páginas <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.paginas.home.index') }}">Home</a></li>
            <li><a href="{{ route('painel.paginas.index', ['somos-diferentes']) }}">Somos Diferentes</a></li>
            <li><a href="{{ route('painel.paginas.index', ['servicos']) }}">Serviços</a></li>
            <li><a href="{{ route('painel.paginas.contato.index') }}">Contato</a></li>
            <li><a href="{{ route('painel.paginas.politica-de-privacidade.index') }}">Política de Privacidade</a></li>
        </ul>
    </li>

    <li @if(str_is('painel.contato.fale-conosco*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.contato.fale-conosco.index') }}">
            Fale conosco
            @if($contatos_nao_lidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatos_nao_lidos }}</span>
            @endif
        </a>
    </li>

    <li @if(str_is('painel.contato.empresa*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.contato.empresa.index') }}">
            Empresa
            @if($empresa_nao_lidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $empresa_nao_lidos }}</span>
            @endif
        </a>
    </li>

    <li @if(str_is('painel.contato.candidato*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.contato.candidato.index') }}">
            Candidatos
            @if($curriculos_nao_lidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $curriculos_nao_lidos }}</span>
            @endif
        </a>
    </li>
</ul>
