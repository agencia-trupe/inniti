@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Editar Página</h2>
    </legend>

    {!! Form::model($pagina, [
        'route'  => ['painel.paginas.update', $pagina->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.paginas.form', ['submitText' => 'Alterar', 'categoria' => $pagina->categoria])

    {!! Form::close() !!}

@endsection
