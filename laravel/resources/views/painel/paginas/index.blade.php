@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>
            {{ $categorias[$categoria] }}
            <a href="{{ route('painel.paginas.create', [$categoria]) }}" class="btn btn-success btn-sm pull-right"><span class="glyphicon glyphicon-plus" style="margin-right:10px;"></span>Adicionar Página</a>
        </h2>
    </legend>

    @if(!count($paginas))
    <div class="alert alert-warning" role="alert">Nenhuma página cadastrada.</div>
    @else
    <table class="table table-striped table-bordered table-hover table-sortable" data-table="paginas">
        <thead>
            <tr>
                <th>Ordenar</th>
                <th>Título</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($paginas as $pagina)
            <tr class="tr-row" id="{{ $pagina->id }}">
                <td>
                    <a href="#" class="btn btn-info btn-sm btn-move">
                        <span class="glyphicon glyphicon-move"></span>
                    </a>
                </td>
                <td>{{ $pagina->titulo }}</td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.paginas.destroy', $pagina->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.paginas.edit', $pagina->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-pencil" style="margin-right:10px;"></span>Editar
                        </a>

                        @if($pagina->slug !== 'metodologia')
                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                        @endif
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    @endif

@endsection
