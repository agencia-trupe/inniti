@include('painel.common.flash')

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('slogan', 'Slogan') !!}
        {!! Form::textarea('slogan', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>

    <div class="form-group col-md-8">
        {!! Form::label('texto', 'Texto') !!}
        {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
    </div>
</div>

<hr>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('chamada1', 'Chamada 1') !!}
        {!! Form::text('chamada1', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-8">
        {!! Form::label('chamada1_frase', 'Chamada 1 - Frase') !!}
        {!! Form::text('chamada1_frase', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('chamada2', 'Chamada 2') !!}
        {!! Form::text('chamada2', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-8">
        {!! Form::label('chamada2_frase', 'Chamada 2 - Frase') !!}
        {!! Form::text('chamada2_frase', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('chamada3', 'Chamada 3') !!}
        {!! Form::text('chamada3', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-8">
        {!! Form::label('chamada3_frase', 'Chamada 3 - Frase') !!}
        {!! Form::text('chamada3_frase', null, ['class' => 'form-control']) !!}
    </div>
</div>

<div class="row">
    <div class="form-group col-md-4">
        {!! Form::label('chamada4', 'Chamada 4') !!}
        {!! Form::text('chamada4', null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-md-8">
        {!! Form::label('chamada4_frase', 'Chamada 4 - Frase') !!}
        {!! Form::text('chamada4_frase', null, ['class' => 'form-control']) !!}
    </div>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
