@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Política de Privacidade</h2>
    </legend>

    {!! Form::model($privacidade, [
        'route'  => ['painel.paginas.politica-de-privacidade.update', $privacidade->id],
        'method' => 'patch'])
    !!}

    @include('painel.paginas.privacidade.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
