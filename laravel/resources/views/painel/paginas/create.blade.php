@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Adicionar Página</h2>
    </legend>

    {!! Form::open(['route' => 'painel.paginas.store', 'files' => true]) !!}

        @include('painel.paginas.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
