@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Empresa</h2>
    </legend>

    <div class="form-group">
        <label>Data</label>
        <div class="well">{{ $contato->created_at }}</div>
    </div>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $contato->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">{{ $contato->email }}</div>
    </div>

    <div class="form-group">
        <label>Empresa</label>
        <div class="well">{{ $contato->empresa }}</div>
    </div>

    @if($contato->telefone)
    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $contato->telefone }}</div>
    </div>
    @endif

    <div class="form-group">
        <label>Mensagem</label>
        <div class="well">{{ $contato->mensagem }}</div>
    </div>

    <a href="{{ route('painel.contato.empresa.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
