@extends('painel.common.template')

@section('content')

    @include('painel.common.flash')

    <legend>
        <h2>Candidatos</h2>
    </legend>

    @if(!count($curriculosrecebidos))
    <div class="alert alert-warning" role="alert">Nenhum currículo recebido.</div>
    @else
    <table class="table table-striped table-bordered table-hover ">
        <thead>
            <tr>
                <th>Nome</th>
                <th>E-mail</th>
                <th>Currículo</th>
                <th><span class="glyphicon glyphicon-cog"></span></th>
            </tr>
        </thead>

        <tbody>
        @foreach ($curriculosrecebidos as $curriculo)

            <tr class="tr-row @if(!$curriculo->lido) warning @endif">
                <td>{{ $curriculo->nome }}</td>
                <td>{{ $curriculo->email }}</td>
                <td><a href="{{ route('curriculo.download', $curriculo->key) }}" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-download" style="margin-right:10px;"></span>Download</a></td>
                <td class="crud-actions">
                    {!! Form::open([
                        'route'  => ['painel.contato.candidato.destroy', $curriculo->id],
                        'method' => 'delete'
                    ]) !!}

                    <div class="btn-group btn-group-sm">
                        <a href="{{ route('painel.contato.candidato.show', $curriculo->id ) }}" class="btn btn-primary btn-sm pull-left">
                            <span class="glyphicon glyphicon-align-left" style="margin-right:10px;"></span>Ficha completa
                        </a>

                        <button type="submit" class="btn btn-danger btn-sm btn-delete"><span class="glyphicon glyphicon-remove" style="margin-right:10px;"></span>Excluir</button>
                    </div>

                    {!! Form::close() !!}
                </td>
            </tr>

        @endforeach
        </tbody>
    </table>
    {!! $curriculosrecebidos->render() !!}
    @endif

@stop
