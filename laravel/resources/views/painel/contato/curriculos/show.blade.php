@extends('painel.common.template')

@section('content')

    <legend>
        <h2>Candidatos</h2>
    </legend>

    <div class="form-group">
        <label>Nome</label>
        <div class="well">{{ $curriculo->nome }}</div>
    </div>

    <div class="form-group">
        <label>E-mail</label>
        <div class="well">{{ $curriculo->email }}</div>
    </div>

    <div class="form-group">
        <label>Telefone</label>
        <div class="well">{{ $curriculo->telefone }}</div>
    </div>

    <div class="form-group">
        <label>Carta de Apresentação</label>
        <div class="well">{{ $curriculo->carta_apresentacao }}</div>
    </div>

    <a href="{{ route('curriculo.download', $curriculo->key) }}" class="btn btn-primary"><span class="glyphicon glyphicon-download" style="margin-right:10px;"></span>Download Currículo</a>

    <a href="{{ route('painel.contato.candidato.index') }}" class="btn btn-default btn-voltar">Voltar</a>

@stop
