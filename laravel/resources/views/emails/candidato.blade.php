<!DOCTYPE html>
<html>
<head>
    <title>[CANDIDATO] {{ config('site.name') }}</title>
    <meta charset="utf-8">
</head>
<body>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Nome:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $nome }}</span><br>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>E-mail:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $email }}</span><br>
    @if($telefone)
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Telefone:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $telefone }}</span><br>
    @endif
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Carta de Apresentação:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'>{{ $carta_apresentacao }}</span>
    <span style='font-weight:bold;font-size:16px;font-family:Verdana;'>Currículo:</span> <span style='color:#000;font-size:14px;font-family:Verdana;'><a href="{{ route('curriculo.download', $key) }}">Fazer Download</a></span>
</body>
</html>
