    <header class="{{ Route::currentRouteName() }}">
        <div class="center nav-principal-wrapper">
            <div class="nav-principal">
                <nav>
                    @include('frontend.common.nav')
                </nav>
            </div>
            <h1><a href="{{ route('home') }}"><img src="{{ asset('assets/img/layout/marca-inniti.png') }}" alt=""></a></h1>
        </div>
    </header>
