    <footer>
        <div class="slogan">
            <div class="center">
                @if(Route::currentRouteName() !== 'home')
                <div>{!! $slogan !!}</div>
                @endif
            </div>
        </div>

        <div class="info">
            <div class="center">
                <div class="left">
                    <div class="col">
                        <h4>Somos Diferentes</h4>
                        <ul>
                        @foreach($paginas as $pagina)
                            @if($pagina->categoria === 'somos-diferentes')
                            <li><a href="{{ route('somos-diferentes', $pagina->slug) }}">{{ $pagina->titulo }}</a></li>
                            @endif
                        @endforeach
                        </ul>
                    </div>
                    <div class="col">
                        <h4>Serviços</h4>
                        <ul>
                        @foreach($paginas as $pagina)
                            @if($pagina->categoria === 'servicos')
                            <li><a href="{{ route('servicos', $pagina->slug) }}">{{ $pagina->titulo }}</a></li>
                            @endif
                        @endforeach
                        </ul>
                    </div>
                </div>

                <div class="right">
                    <div class="marca"></div>
                    <div class="contato">
                        @if($contato->linkedin)
                        <a href="{{ $contato->linkedin }}" class="linkedin" target="_blank">linkedin</a>
                        @endif
                        <p class="telefone">{{ $contato->telefone }}</p>
                        <a href="mailto:{{ $contato->email }}" class="email">{{ $contato->email }}</a>
                        <div class="endereco">
                            {!! $contato->endereco !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="copyright">
            <div class="center">
                <p>
                    © {{ date('Y') }} {{ config('site.name') }} - Todos os direitos reservados. ·
                    <a href="{{ route('politica-de-privacidade') }}" class="politica">Conheça nossa Política de Privacidade</a>
                </p>
                <p>
                    <a href="http://trupe.net" target="_blank">Criação de sites</a>:
                    <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a>
                </p>
            </div>
        </div>
    </footer>
