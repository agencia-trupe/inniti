    <div class="header-mobile">
        <div class="center">
            <h1>
                <a href="{{ route('home') }}"><img src="{{ asset('assets/img/layout/marca-inniti.png') }}" alt=""></a>
            </h1>
            <button id="mobile-toggle" type="button" role="button">
                <span class="lines"></span>
            </button>
        </div>
    </div>
    <nav class="mobile">
        <a href="{{ route('home') }}" @if(Route::currentRouteName() === 'home') class="active" @endif>Home</a>
        @include('frontend.common.nav')
    </nav>
