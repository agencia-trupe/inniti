    <header class="home">
        <div class="banners">
        @foreach($banners as $banner)
            <div class="banner" style="background-image: url('{{ asset('assets/img/banners/'.$banner->imagem) }}');" data-link="{{ $banner->link }}">
                <div class="center">
                    <div class="banner-texto-wrapper">
                        <div class="banner-texto">
                            <span class="titulo">{{ $banner->titulo }}</span>
                            <span class="frase">{{ $banner->frase }}</span>
                        </div>
                    </div>
                </div>
                <a href="{{ $banner->link }}" class="banner-link"></a>
            </div>
        @endforeach
            <div class="cycle-pager"></div>
        </div>

        <div class="center nav-principal-wrapper">
            <div class="nav-principal">
                <nav>
                    @include('frontend.common.nav')
                </nav>
                <h1><a href="{{ route('home') }}"><img src="{{ asset('assets/img/layout/marca-inniti.png') }}" alt=""></a></h1>
            </div>
            <a href="#" id="banner-link-outside"></a>
        </div>
    </header>
