<a href="{{ route('somos-diferentes') }}" @if(Route::currentRouteName() === 'somos-diferentes') class="active" @endif>Somos Diferentes</a>
<a href="{{ route('servicos') }}" @if(Route::currentRouteName() === 'servicos') class="active" @endif>Serviços</a>
<a href="{{ route('contato') }}" @if(Route::currentRouteName() === 'contato') class="active" @endif>Contato</a>
