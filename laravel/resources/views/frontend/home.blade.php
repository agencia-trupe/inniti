@extends('frontend.common.template')

@section('content')

    <div class="main home">
        <div class="center">
            <div class="textos">
                <div class="slogan">
                    <div class="slogan-wrapper">
                        <div>{!! $home->slogan !!}</div>
                    </div>
                </div>

                <div class="texto">
                    {!! $home->texto !!}
                    <div class="triangulo"></div>
                </div>
            </div>

            <div class="chamadas">
                @foreach(range(1,4) as $i)
                <div class="col">
                    <h4>{{ $home->{'chamada'.$i} }}</h4>
                    <p>{{ $home->{'chamada'.$i.'_frase'} }}</p>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
