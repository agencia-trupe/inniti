@extends('frontend.common.template')

@section('content')

    <div class="main paginas">
        <div class="center">
            @include('frontend.contato.aside')

            <article class="contato">
                <h2>Candidato</h2>
                <p>Envie seu currículo para participar de nosso Banco de Talentos.</p>

                <form action="{{ route('contato.envio.candidato') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @if(session('success'))
                    <div class="response success">
                        Currículo enviado com sucesso!
                    </div>
                    @elseif (count($errors) > 0)
                    <div class="response error">
                        @foreach($errors->all() as $error)
                        <p>{!! $error !!}</p>
                        @endforeach
                    </div>
                    @endif

                    <input type="text" name="nome" id="nome" placeholder="nome" value="{{ old('nome') }}" @if($errors->has('nome'))class="error"@endif>
                    <input type="email" name="email" id="email" placeholder="e-mail" value="{{ old('email') }}" @if($errors->has('email'))class="error"@endif>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone" value="{{ old('telefone') }}" @if($errors->has('telefone'))class="error"@endif>
                    <textarea name="carta_apresentacao" id="carta_apresentacao" placeholder="carta de apresentação" @if($errors->has('carta_apresentacao'))class="error"@endif>{{ old('carta_apresentacao') }}</textarea>
                    <label id="curriculo" @if($errors->has('curriculo'))class="error"@endif>
                        <span>ANEXAR CURRÍCULO</span>
                        <input type="file" name="curriculo">
                    </label>
                    <input type="submit" value="ENVIAR">
                </form>
            </article>
        </div>
    </div>

@endsection
