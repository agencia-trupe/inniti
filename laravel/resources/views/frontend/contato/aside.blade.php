<aside>
    <a href="{{ route('contato', 'fale-conosco') }}" @if($pagina === 'fale-conosco') class="active" @endif>Fale Conosco</a>
    <a href="{{ route('contato', 'empresa') }}" @if($pagina === 'empresa') class="active" @endif>Empresa</a>
    <a href="{{ route('contato', 'candidato') }}" @if($pagina === 'candidato') class="active" @endif>Candidato</a>
</aside>
