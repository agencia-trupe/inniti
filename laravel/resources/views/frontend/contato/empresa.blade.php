@extends('frontend.common.template')

@section('content')

    <div class="main paginas">
        <div class="center">
            @include('frontend.contato.aside')

            <article class="contato">
                <h2>Empresa</h2>

                <form action="{{ route('contato.envio.empresa') }}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    @if(session('success'))
                    <div class="response success">
                        Mensagem enviada com sucesso!
                    </div>
                    @elseif (count($errors) > 0)
                    <div class="response error">
                        @foreach($errors->all() as $error)
                        <p>{!! $error !!}</p>
                        @endforeach
                    </div>
                    @endif

                    <input type="text" name="nome" id="nome" placeholder="nome" value="{{ old('nome') }}" @if($errors->has('nome'))class="error"@endif>
                    <input type="email" name="email" id="email" placeholder="e-mail" value="{{ old('email') }}" @if($errors->has('email'))class="error"@endif>
                    <input type="text" name="empresa" id="empresa" placeholder="empresa" value="{{ old('empresa') }}" @if($errors->has('empresa'))class="error"@endif>
                    <input type="text" name="telefone" id="telefone" placeholder="telefone" value="{{ old('telefone') }}" @if($errors->has('telefone'))class="error"@endif>
                    <textarea name="mensagem" id="mensagem" placeholder="mensagem" @if($errors->has('mensagem'))class="error"@endif>{{ old('mensagem') }}</textarea>
                    <input type="submit" value="ENVIAR">
                </form>
            </article>
        </div>
    </div>

@endsection
