@extends('frontend.common.template')

@section('content')

    <div class="main paginas">
        <div class="center">
            <aside>
                @foreach($paginas as $link)
                <a href="{{ route($categoria, $link->slug) }}" @if($pagina->slug === $link->slug) class="active" @endif>{{ $link->titulo }}</a>
                @endforeach
            </aside>

            <article class="{{ Route::currentRouteName() }} {{ $pagina->slug }}">
                <h2>{{ $pagina->titulo }}</h2>
                <div class="texto paginas-texto">
                    {!! $pagina->texto !!}

                    @if($pagina->slug === 'metodologia' && $categoria === 'somos-diferentes')
                        @include('frontend.metodologia')
                    @endif
                </div>
            </article>
        </div>
    </div>

@endsection
