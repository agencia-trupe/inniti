@extends('frontend.common.template')

@section('content')

    <div class="main paginas">
        <div class="center">
            <article class="privacidade">
                <h2>Política de Privacidade</h2>
                {!! $privacidade->texto !!}
            </article>
        </div>
    </div>

@endsection
