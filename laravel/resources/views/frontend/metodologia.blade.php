<div class="grafico-wrapper">
    <div class="desktop">
        <div class="grafico grafico-1"></div>
        <div class="grafico grafico-2"></div>
        <div class="grafico grafico-4"></div>
        <div class="grafico grafico-3"></div>
    </div>
    <div class="mobile">
        <p>
            <img src="{{ asset('assets/img/layout/grafico-metodologia.png') }}" alt="">
        </p>
    </div>
</div>
