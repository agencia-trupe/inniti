var config      = require('../config'),
    gulp        = require('gulp'),
    stylus      = require('gulp-stylus'),
    jeet        = require('jeet'),
    rupture     = require('rupture'),
    koutoSwiss  = require('kouto-swiss'),
    plumber     = require('gulp-plumber'),
    browserSync = require('browser-sync');

gulp.task('stylus', function() {
    return gulp.src([config.development.stylus + 'main.styl'])
        .pipe(plumber())
        .pipe(stylus({
            use: [koutoSwiss(), jeet(), rupture()],
            compress: true
        }))
        .pipe(gulp.dest(config.build.css))
        .pipe(browserSync.reload({ stream: true }))
});
