-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 04-Set-2015 às 15:59
-- Versão do servidor: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `inniti`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `banners`
--

CREATE TABLE IF NOT EXISTS `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Extraindo dados da tabela `banners`
--

INSERT INTO `banners` (`id`, `ordem`, `imagem`, `titulo`, `frase`, `created_at`, `updated_at`) VALUES
(1, 0, '20150904133320_img-banner1.jpg', 'Executive Search', 'especialistas em práticas e disciplinas específicas', '2015-09-04 16:33:21', '2015-09-04 16:33:21'),
(2, 1, '20150904133427_img-banner2.jpg', 'Corporate Governance', 'profundo conhecimento de governança', '2015-09-04 16:34:27', '2015-09-04 16:34:27'),
(3, 2, '20150904133457_img-banner3.jpg', 'Desenvolvimento Organizacional', 'diagnóstico, desenho e implantação de soluções - conectando pessoas e estratégias', '2015-09-04 16:34:57', '2015-09-04 16:34:57'),
(4, 3, '20150904133523_img-banner4.jpg', 'Coaching e Mentoring', 'equipes de coaches e mentores para cada situação e objetivo', '2015-09-04 16:35:24', '2015-09-04 16:35:24'),
(5, 4, '20150904133558_img-banner5.jpg', 'Lectures | Palestras', 'difusão de conhecimento (inovador) para o mercado', '2015-09-04 16:35:59', '2015-09-04 16:35:59');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE IF NOT EXISTS `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `linkedin` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `googlemaps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `telefone`, `email`, `endereco`, `linkedin`, `googlemaps`, `created_at`, `updated_at`) VALUES
(1, '+55 11 3568 2590', 'contato@inniti.com.br', '<p>Av. Brigadeiro Faria Lima, 3144&nbsp;&middot; 30&ordm; andar</p><p>Itaim Bibi&nbsp;&middot; S&atilde;o Paulo SP&nbsp;&middot; 01451-001&nbsp;&middot; Brasil</p>', 'https://br.linkedin.com/company/teperman', '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d117079.5550434779!2d-46.602918949999996!3d-23.506013249999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5769dddfab03%3A0x284d9c6a4f6013b6!2sAv.+Brg.+Faria+Lima%2C+3144+-+Jardim+Paulistano%2C+S%C3%A3o+Paulo+-+SP!5e0!3m2!1spt-BR!2sbr!4v1441298504634" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE IF NOT EXISTS `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `curriculos_recebidos`
--

CREATE TABLE IF NOT EXISTS `curriculos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `carta_apresentacao` text COLLATE utf8_unicode_ci NOT NULL,
  `curriculo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `home`
--

CREATE TABLE IF NOT EXISTS `home` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `slogan` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `chamada1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada1_frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada2_frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada3_frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada4` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `chamada4_frase` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `home`
--

INSERT INTO `home` (`id`, `slogan`, `texto`, `chamada1`, `chamada1_frase`, `chamada2`, `chamada2_frase`, `chamada3`, `chamada3_frase`, `chamada4`, `chamada4_frase`, `created_at`, `updated_at`) VALUES
(1, '<p><strong>Connect</strong> to align.</p><p>Align to <strong>succeed</strong>.</p>', '<p><em>Acreditamos que o <strong>autoconhecimento </strong>&eacute; capaz de transformar empresas e profissionais. E &eacute; a partir desse entendimento que os auxiliamos a analisar e compreender o que de fato <strong>desejam</strong>, <strong>precisam </strong>e, principalmente, no que <strong>realmente acreditam</strong>.</em></p><p><em>O resultado &eacute; um <strong>perfeito alinhamento de prop&oacute;sitos</strong> entre todas as partes.</em></p>', 'Autenticidade e Conhecimento', 'para entender que uma empresa é feita, acima de tudo, de pessoas', 'Propósito e Precisão', 'para alinhar pessoas e negócios', 'Cumplicidade e Confiança', 'para gerar valor para todas as partes', 'Transparência', 'para unir objetivos e expectativas', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_08_31_124252_create_banners_table', 1),
('2015_08_31_124320_create_home_table', 1),
('2015_08_31_124638_create_paginas_table', 1),
('2015_08_31_124741_create_contato_table', 1),
('2015_08_31_124749_create_contatos_recebidos_table', 1),
('2015_09_01_141133_create_curriculos_recebidos_table', 1),
('2015_09_01_173231_create_privacidade_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas`
--

CREATE TABLE IF NOT EXISTS `paginas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `categoria` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `paginas_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `paginas`
--

INSERT INTO `paginas` (`id`, `ordem`, `categoria`, `titulo`, `slug`, `texto`, `created_at`, `updated_at`) VALUES
(1, 0, 'somos-diferentes', 'Razão de ser', 'razao-de-ser', '<p>Acreditamos que o <strong>autoconhecimento</strong> &eacute; capaz de transformar empresas e profissionais. E &eacute; a partir desse entendimento que os auxiliamos a analisar e compreender o que de fato <strong>desejam</strong>, <strong>precisam</strong> e, principalmente, no que <strong>realmente acreditam</strong>.</p>\r\n\r\n<p>O resultado &eacute; um <strong>perfeito alinhamento de prop&oacute;sitos</strong> entre todas as partes.</p>\r\n\r\n<p class="olho">Somos parceiros de nossos clientes no recrutamento de executivos, gest&atilde;o de equipes e l&iacute;deres, diminui&ccedil;&atilde;o de <em>turnover</em>, aumento da reten&ccedil;&atilde;o de profissionais, desenvolvimento de talentos e condu&ccedil;&atilde;o do processo de sucess&atilde;o na empresa, tendo sempre como objetivo gerar valor para todos.</p>\r\n\r\n<p><img src="http://inniti.dev/assets/img/paginas/20150904134715_img-razaodeser.jpg" /></p>\r\n', '2015-09-04 16:45:49', '2015-09-04 16:47:55'),
(2, 0, 'servicos', 'Coaching e Desenvolvimento de Líderes', 'coaching-e-desenvolvimento-de-lideres', '<p class="olho"><em>Equipes de coaches e mentores para cada situa&ccedil;&atilde;o e objetivo</em></p>\r\n\r\n<ul>\r\n	<li>Coaching individual</li>\r\n	<li>Coaching para times</li>\r\n	<li>Mentoramento</li>\r\n	<li>Forma&ccedil;&atilde;o de Mentores: projetos de implanta&ccedil;&atilde;o e gest&atilde;o de Programa de Mentores</li>\r\n	<li>Trainees e Estagi&aacute;rios: programas espec&iacute;ficos para capacita&ccedil;&atilde;o de l&iacute;deres</li>\r\n</ul>\r\n\r\n<p><img src="http://inniti.dev/assets/img/paginas/20150904135525_praticas-inniti.png" /></p>\r\n', '2015-09-04 16:55:31', '2015-09-04 16:55:31');

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `privacidade`
--

CREATE TABLE IF NOT EXISTS `privacidade` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `privacidade`
--

INSERT INTO `privacidade` (`id`, `texto`, `created_at`, `updated_at`) VALUES
(1, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus molestie, enim sed fringilla pulvinar, magna nisi bibendum purus, eget mattis est eros sit amet lectus. Sed interdum urna et tortor lobortis, eu suscipit elit sagittis. Morbi tempor, sem ut sagittis congue, nisl justo volutpat lectus, quis aliquam erat turpis in erat. Pellentesque mattis vel ipsum a dignissim. Nullam at tristique mauris, ut pharetra magna. Sed cursus nisi at enim lobortis, nec ornare nibh blandit. Aenean a pulvinar quam. Sed eget ex consectetur leo aliquet blandit pulvinar ac ex. Mauris fermentum semper quam a rutrum. Aliquam faucibus porta est, in consectetur augue sollicitudin ac. Nam vestibulum metus tempus, eleifend tellus sit amet, faucibus ipsum. Cras aliquet ante a lectus porta, id varius elit eleifend. Phasellus scelerisque dui sit amet dignissim mattis. Nullam a odio porta, convallis sem sit amet, accumsan leo. Phasellus tempus purus et mi malesuada congue.</p>\r\n\r\n<p>Proin nibh est, consequat a arcu ut, dignissim posuere erat. Nunc tincidunt nulla dictum lorem dignissim, id viverra elit efficitur. Proin nec fringilla sem. Aenean dictum vehicula tortor, sollicitudin finibus lorem sagittis non. Suspendisse potenti. Duis nec purus rhoncus, egestas justo nec, semper quam. Aliquam sed arcu metus. Phasellus ornare massa felis, at ultricies elit scelerisque non. Mauris in lorem vitae purus facilisis sodales at id nunc. Maecenas semper velit enim, a dapibus nisi condimentum vel. Integer vel orci ullamcorper nisi volutpat sollicitudin in in orci. Nam sit amet nibh ut turpis vestibulum porttitor. Fusce in ipsum elit. In molestie massa et neque ultrices varius. Nam bibendum ipsum at nunc aliquet molestie.</p>\r\n\r\n<p>Suspendisse id ex cursus mi malesuada finibus. Maecenas vehicula bibendum leo quis porta. Mauris suscipit, turpis non maximus maximus, urna nisl auctor eros, sit amet dapibus arcu nulla in turpis. Morbi at ante libero. Etiam vel tellus quis lorem iaculis lobortis fermentum vitae nunc. Integer nibh nisl, pellentesque vitae ornare nec, feugiat quis ex. Praesent congue sit amet est nec venenatis. Aenean auctor mi augue, ac porta leo aliquet vitae. Aliquam dictum sit amet mi ut hendrerit. Vestibulum sit amet placerat diam, eu venenatis eros. Sed sit amet mauris sit amet risus blandit varius. Cras vel pharetra lacus, ac hendrerit nisl. Nam ut facilisis sem, cursus ullamcorper sapien. Maecenas ut risus magna. Quisque lobortis erat risus, congue hendrerit metus semper at. Nulla vitae nunc quis mi blandit lacinia vel ac leo.</p>\r\n\r\n<p>Integer mollis, nisi id ornare lacinia, libero lectus pretium enim, sed eleifend diam libero eu libero. Fusce pharetra, nisl sed vulputate sagittis, est velit consectetur nibh, at suscipit lorem quam vitae odio. Aenean massa felis, malesuada eu nibh ut, congue hendrerit urna. In faucibus erat eget enim ultrices dignissim eget id libero. Vestibulum finibus suscipit augue, volutpat lobortis purus. Maecenas eget tellus nisl. Aliquam leo magna, bibendum vel sem lacinia, cursus euismod tortor. Etiam vel gravida felis. Vestibulum nibh ex, pretium ac velit ut, luctus molestie eros. Nulla mattis magna ut libero molestie, eget tincidunt leo sagittis. Maecenas sed porttitor risus. Integer dapibus hendrerit tortor sed semper. Vivamus dapibus nunc efficitur risus facilisis, placerat auctor ante ornare.</p>\r\n\r\n<p>Aenean fringilla dolor vel vulputate hendrerit. Donec ultricies felis vel porta lobortis. Proin a justo enim. Curabitur metus augue, sodales non fermentum at, tristique sagittis nisl. Vivamus faucibus viverra neque, a lobortis arcu viverra eget. Vestibulum vitae risus at diam consequat dictum. Fusce aliquam nunc id facilisis pretium. Sed diam leo, commodo in lacinia lacinia, elementum non odio. Sed imperdiet, mi vel volutpat semper, eros diam tincidunt turpis, vehicula eleifend tortor est quis lacus. Suspendisse interdum sagittis turpis sed scelerisque. Curabitur vel scelerisque odio. Phasellus aliquam vehicula mauris vel maximus. Integer odio nulla, eleifend eget sapien non, varius pulvinar purus.</p>\r\n', '0000-00-00 00:00:00', '2015-09-04 16:48:41');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$lIW99VPRL39cLLw6YmI9OOKA4RlxLQ5bhVBz.s8vvPKZajPwbrFE6', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
